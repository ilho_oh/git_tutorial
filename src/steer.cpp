#include <iostream>
#include "steer.h"

namespace steer_interface{
double get_steer(){
    std::cout << "steer_interface::get_steer() called"<<std::endl;
    return 0.0;
}
void set_steer(double deg){
    std::cout << "steer_interface::set_steer( double ) called with " << deg <<std::endl;
}
}
