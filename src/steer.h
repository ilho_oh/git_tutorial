#ifndef STEER_H__
#define STEER_H__

namespace steer_interface{
double get_steer();
void set_steer(double deg);
}
#endif //STEER_H__